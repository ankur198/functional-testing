package com.nagarro.driven.runner.junit.base;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.TestReporter;

public class JunitTestReporter {

  TestInfo testinfo;
  TestReporter testReporter;

  @BeforeAll
  static void initAll() {
    // Unused
  }

  @BeforeEach
  public void init(TestInfo testinfo, TestReporter testReporter) {
    this.testinfo = testinfo;
    this.testReporter = testReporter;
  }

  @AfterAll
  static void tearDownAll() {
    // Unused
  }

  @AfterEach
  public void tearDown() {
    // Unused
  }
}
