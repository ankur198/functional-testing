package com.nagarro.driven.runner.junit.base;

import com.nagarro.driven.core.reporting.api.IReportManager;
import com.nagarro.driven.core.util.Cleanable;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.lang.reflect.Method;
import java.util.Optional;

public abstract class JunitTestBase extends JunitTestReporter {
  private static final Logger log = LoggerFactory.getLogger(JunitTestBase.class);
  @Inject private IReportManager reportManager;

  /**
   * Gets the report manager class.
   *
   * @return the reporter class.
   */
  public abstract Class<?> getReportManager();

  /** Creates the parent node in report. */
  @BeforeAll // (alwaysRun = true)
  public synchronized void beforeAll() {
    String parentName = getClass().getSimpleName();
    log.info("Creating the parent node {} in report.", parentName);
    reportManager.createNode(parentName, true, false);
  }

  /**
   * Creates the child node inside the parent node created in beforeTestClass method.
   *
   * @param info, Test Information
   */
  @BeforeEach
  public void setUpBase(TestInfo info) {
    Optional<Method> optionalMethod = info.getTestMethod();
    if (optionalMethod.isPresent()) {
      Method method = optionalMethod.get();
      log.info("Creating the child node {} in report.", method.getName());
      reportManager.createNode(method.getName(), false, false);
    }
  }

  /** Flush and close the report. */
  @AfterAll // (alwaysRun = true)
  public void flushAndCloseReport() {
    reportManager.getReporter().flush();
  }

  /**
   * Trigger the clean up action.
   *
   * @param cleanables
   */
  protected void triggerAppropriateCleanUps(Cleanable... cleanables) {
    for (Cleanable cleanable : cleanables) {
      try {
        cleanable.cleanUp();
      } catch (Exception cleanUpException) {
        cleanable.emergencyCleanUp();
      }
    }
  }
}
