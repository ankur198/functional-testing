package com.nagarro.driven.project.espo.test.test;

import com.nagarro.driven.project.espo.test.base.EspoPOMTestBase;
import com.nagarro.driven.project.espo.test.pageobjects.HomePage;
import com.nagarro.driven.project.espo.test.pageobjects.LoginPage;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class HomePageTest extends EspoPOMTestBase {

  private LoginPage login;
  private HomePage home;

  @Test
  public void verifyCalenderTile() {
    home.addCalender();
    Assert.assertTrue(home.isCalenderTileDisplayed(), "is Calender Tile Displayed ?");
  }

  @BeforeMethod
  public void userLogin() {
    login = new LoginPage(seleniumAbstractDriverProvider.get());
    home = new HomePage(seleniumAbstractDriverProvider.get());
    login.loginToEspo("admin", "nagarro@123");
  }

  @AfterMethod
  public void userLogout() {
    login.appLogout();
  }
}
