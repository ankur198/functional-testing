package com.nagarro.driven.project.espo.test.test;

import com.nagarro.driven.project.espo.data.TestDataProvider;
import com.nagarro.driven.project.espo.test.base.EspoPOMTestBase;
import com.nagarro.driven.project.espo.test.pageobjects.LoginPage;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LoginTest extends EspoPOMTestBase {

  private LoginPage login;

  @Test(dataProvider = "TestData", dataProviderClass = TestDataProvider.class)
  public void verifyLogin(String username, String password, String pagetitle) {
    login.loginToEspo(username, password);
    Assert.assertEquals(login.getPageTitle(), pagetitle);
  }

  @Test(dataProvider = "TestData", dataProviderClass = TestDataProvider.class)
  public void invalidCredentials(String username, String password, String pagetitle) {
    login.loginToEspo(username, password);
    Assert.assertTrue(login.isLoginButtonDisplayed());
  }

  @AfterMethod
  public void userLogoutIfNeeded() {
    if (!login.isLoginButtonDisplayed()) {
      login.appLogout();
    }
  }

  @BeforeMethod
  public void userLogin() {
    login = new LoginPage(seleniumAbstractDriverProvider.get());
  }
}
