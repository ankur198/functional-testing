package com.nagarro.driven.project.espo.test.test;

import com.nagarro.driven.project.espo.data.TestDataProvider;
import com.nagarro.driven.project.espo.test.base.EspoPOMTestBase;
import com.nagarro.driven.project.espo.test.pageobjects.AccountsPage;
import com.nagarro.driven.project.espo.test.pageobjects.HomePage;
import com.nagarro.driven.project.espo.test.pageobjects.LoginPage;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class AccountsTest extends EspoPOMTestBase {

  private LoginPage login;
  private HomePage home;
  private AccountsPage accounts;

  @Test(dataProvider = "TestData", dataProviderClass = TestDataProvider.class)
  public void verifySearchTextOnAccounts(String username, String password, String account) {
    login.loginToEspo(username, password);
    home.clickAccountsOption();
    accounts.searchAccounts(account);
    accounts.openSearchedData();
    String nameOfAccount = accounts.getOverviewDisplayedNameData();
    Assert.assertEquals(nameOfAccount, account);
  }

  @Test(dataProvider = "TestData", dataProviderClass = TestDataProvider.class)
  public void updateDetailsOnOverview(
      String username,
      String password,
      String account,
      String address,
      String phone,
      String website) {
    login.loginToEspo(username, password);
    home.clickAccountsOption();
    accounts.searchAccounts(account);
    accounts.openSearchedData();
    accounts.updateDetailsInOverview(address, phone, website);
    String actual = accounts.getOverviewDisplayedBillingAddress();
    Assert.assertTrue(actual.contains(address));
  }

  /** User account = 'Mona Mary' * */
  @Test(dataProvider = "TestData", dataProviderClass = TestDataProvider.class)
  public void verifyEditedWebsiteUrlForUserAccount(
      String username, String password, String website) {
    login.loginToEspo(username, password);
    home.clickAccountsOption();
    accounts.searchAndClickUserAccount();
    accounts.editWebSiteOnGUI(website);
    final String actual = accounts.getWebSiteUrl();
    Assert.assertEquals(actual, "nagarro.com");
  }

  @BeforeMethod
  public void userLogin() {
    login = new LoginPage(seleniumAbstractDriverProvider.get());
    home = new HomePage(seleniumAbstractDriverProvider.get());
    accounts = new AccountsPage(seleniumAbstractDriverProvider.get());
  }

  @AfterMethod
  public void userLogout() {
    login.appLogout();
  }
}
