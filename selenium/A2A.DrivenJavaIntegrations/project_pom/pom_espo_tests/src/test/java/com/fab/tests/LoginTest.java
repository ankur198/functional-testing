package com.fab.tests;

import com.fab.pageObjects.LoginPage;
import com.nagarro.driven.project.espo.data.TestDataProviderFab;
import com.nagarro.driven.project.espo.test.base.EspoPOMTestBase;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LoginTest extends EspoPOMTestBase {
    private LoginPage login;

    @Test(dataProvider = "TestData", dataProviderClass = TestDataProviderFab.class)
    public void verifyLogin(String username, String password, String pagetitle) {
        login.loginToNSC(username, password);
        Assert.assertEquals(login.getPageTitle(), pagetitle);
    }

    @Test(dataProvider = "TestData", dataProviderClass = TestDataProviderFab.class)
    public void invalidCredentials(String username, String password, String pagetitle) {
        login.loginToNSC(username, password);
        Assert.assertTrue(login.isSignInButtonDisplayed());
    }

    @BeforeMethod
    public void userLogin() {
        login = new LoginPage(seleniumAbstractDriverProvider.get());
    }

    @AfterMethod
    public void userLogoutIfNeeded() {
        if (!login.isSignInButtonDisplayed()) {
            login.appLogout();
        }
    }
}


