package com.fab.tests;

import com.fab.pageObjects.HomePage;
import com.fab.pageObjects.LoginPage;
import com.nagarro.driven.project.espo.data.TestDataProviderFab;
import com.nagarro.driven.project.espo.test.base.EspoPOMTestBase;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class HomeTest extends EspoPOMTestBase {
    private LoginPage login;
    private HomePage home;

    /** Verify whether all sections are displayed on "Amazon Web Services" dashboard or not */
    @Test
    public void verifySectionsOnDashboard() {
        home.clickAwsLink();
        Assert.assertTrue(home.isInstanceAvailabilityTrendDisplayed(), "is instance availability trend displayed?");
        Assert.assertTrue(home.isServerLocationsDisplayed(), "is server locations displayed?");
        Assert.assertTrue(home.isSecurityThreadCountForProjectsDisplayed(), "is security thread count for projects displayed?");
        Assert.assertTrue(home.isServicevsSecurityThreatsCountDisplayed(), "is service vs security threats count displayed?");
        Assert.assertTrue(home.isAccountCostDistributionDisplayed(), "is account cost distribution displayed?");
        Assert.assertTrue(home.isAWSAccountsDisplayed(), "is aws accounts displayed?");
        Assert.assertTrue(home.isAccountInstanceFilteringDisplayed(), "is account instance filtering displayed?");
        Assert.assertTrue(home.isSecurityThreatsCountForRegionsDisplayed(), "is security threats count for regions displayed?");
    }

    /** Verify whether all sections are displayed on "Nagarro Devops" dashboard or not */
     @Test
     public  void verifySectionsOnNagarroDevopsDashboard() throws InterruptedException {
         home.clickAwsLink();
         Thread.sleep(2000);
         home.clickAWSAccountsLink();
         Thread.sleep(2000);
         home.clickNagarroDevOpsButton();
         Assert.assertTrue(home.isServerTypeDisplayed(), "is server type displayed?");
         Assert.assertTrue(home.isAccountConfigurationDisplayed(), "is account configuration displayed?");
         Assert.assertTrue(home.isCostOptimizationDisplayed(), "is cost optimization displayed?");
         Assert.assertTrue(home.isServiceCostTrendDisplayed(), "is service cost trend displayed?");
         Assert.assertTrue(home.isTagsDummyDisplayed(), "is tags dummy displayed?");
         Assert.assertTrue(home.isSecurityBreachesDummyDisplayed(), "is security breaches dummy displayed?");
         Assert.assertTrue(home.isDailyCostTrendDisplayed(), "is daily cost trend displayed?");
     }

     /** To verify if user is able to add aws account*/
    @Test(dataProvider = "TestData", dataProviderClass = TestDataProviderFab.class)
    public  void verifyAddAwsAccount(String accountName,String accountId, String accessKey, String secretKey) throws InterruptedException {
        home.clickAwsLink();
        Thread.sleep(5000);
        home.clickAWSAccountsLink();
        Thread.sleep(5000);
        home.clickOnAddAwsAccountButton();
        home.enterAccountNameData(accountName);
        home.enterAccountIdData(accountId);
        home.enteAccessKeyData(accessKey);
        home.enterSecretKeyData(secretKey);
        Thread.sleep(2000);
        home.clickOnAddButton();
       Assert.assertTrue(home.isSuccessMsgDisplayed(),"is aws account added successfully?");

    }

    /** To verify data on year old assets widget*/
    @Test(dataProvider = "TestData", dataProviderClass = TestDataProviderFab.class)
      public  void verifyDataOnYearOldAssetsWidget(String ami, String ec2, String db) throws InterruptedException {
        home.clickAwsLink();
        Thread.sleep(2000);
        home.clickAWSAccountsLink();
        Thread.sleep(2000);
        home.clickNagarroDevOpsButton();
        Thread.sleep(2000);
        home.clickCostOptimizationWidget();
       // String ami = home.getAmiValue();
        Assert.assertEquals(home.getAmiValue(),ami);
        Assert.assertEquals(home.getEc2SnapshotValue(),ec2);
        Assert.assertEquals(home.getDbSnapshotValue(),db);
        Thread.sleep(2000);

    }

    /**  To verify ami name and details*/
    @Test(dataProvider = "TestData", dataProviderClass = TestDataProviderFab.class)
    public  void verifyAmiNameAndDetails(String type, String id, String name, String region, String managementUrl,String sku, String date) throws InterruptedException {
        home.clickAwsLink();
        Thread.sleep(2000);
        home.clickAWSAccountsLink();
        Thread.sleep(2000);
        home.clickNagarroDevOpsButton();
        Thread.sleep(2000);
        home.clickCostOptimizationWidget();
        Thread.sleep(2000);
        home.clickOnAmi();
        Thread.sleep(2000);
        home.clickOnAnomaly();
        Assert.assertEquals(home.getAmiTypeValue(),type);
        Assert.assertEquals(home.getAmiIdValue(),id);
        Assert.assertEquals(home.getAmiNameValue(),name);
        Assert.assertEquals(home.getAmiRegionValue(),region);
        Assert.assertEquals(home.getManagementUrlValue(),managementUrl);
        Assert.assertEquals(home.getSkuValue(),sku);
        Assert.assertEquals(home.getDateValue(),date);


    }

    @BeforeMethod
    public void userLogin() {
        login = new LoginPage(seleniumAbstractDriverProvider.get());
        home = new HomePage(seleniumAbstractDriverProvider.get());
        login.loginToNSC("user1@nagarro.com", "user1demo");
    }

    @AfterMethod
    public void userLogout() {
        login.appLogout();
    }
}

