package com.nagarro.driven.project.espo.data;

import com.nagarro.driven.core.rest.api.ApiUtil;
import com.nagarro.driven.core.util.CSVutil;
import com.nagarro.driven.project.espo.data.config.DataConfigHolder;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class AccountsDataProvider {

  private static final Logger LOG = LoggerFactory.getLogger(AccountsDataProvider.class);
  private static final String FILE_PATH = DataConfigHolder.getInstance().csvFilePath();
  private static final String USER_DIRECTORY =
          Paths.get(System.getProperty("user.dir")).getParent().toString();
  private static final String FILE_NAME = "Accounts.csv";
  private static final String DATA_FILE = USER_DIRECTORY + FILE_PATH + FILE_NAME;

  private static final String JSON_FILE_NAME = "accounts.json";
  private static final String JSON_DATA_FILE = USER_DIRECTORY + FILE_PATH + JSON_FILE_NAME;


  public List<String[]> readAccountsFromCSV() {
    List<String[]> data = null;
    try {
      LOG.info("Reading the data from the file ;{}", FILE_NAME);
      data = CSVutil.reader("", DataConfigHolder.getInstance().csvDelimiter(), DATA_FILE);
    } catch (Exception e) {
      LOG.error(String.format("Could not load data or entire file <%s>", FILE_NAME), e);
    }
    return data;
  }

}