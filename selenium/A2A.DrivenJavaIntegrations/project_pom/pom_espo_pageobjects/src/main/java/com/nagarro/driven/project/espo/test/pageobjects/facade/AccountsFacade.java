package com.nagarro.driven.project.espo.test.pageobjects.facade;

import com.nagarro.driven.client.selenium.SeleniumAbstractDriver;
import com.nagarro.driven.project.espo.test.pageobjects.AccountsPage;
import com.nagarro.driven.project.espo.test.pageobjects.HomePage;
import com.nagarro.driven.project.espo.test.pageobjects.LoginPage;

public class AccountsFacade extends LoginPage {

    public AccountsFacade(SeleniumAbstractDriver client) {
        super(client);
    }

    private LoginPage loginPage;
    private HomePage homePage;
    private AccountsPage accountsPage;

    public LoginPage loginPage() {
        if (loginPage == null) {
            loginPage = new LoginPage(seleniumClient);
        }

        return loginPage;
    }

    public HomePage homePage() {
        if (homePage == null) {
            homePage = new HomePage(seleniumClient);
        }

        return homePage;
    }

    public AccountsPage accountsPage() {
        if (accountsPage == null) {
            accountsPage = new AccountsPage(seleniumClient);
        }

        return accountsPage;
    }

    public String searchedMyHealthText(String searchedText) {
        homePage().clickAccountsOption();
        accountsPage().searchAccounts(searchedText);
        return accountsPage().getSearchedText();
    }

    public String nameOnOverview() {
        accountsPage().clickMyHealthOption();
        return accountsPage().getOverviewDisplayedNameData();
    }
}
