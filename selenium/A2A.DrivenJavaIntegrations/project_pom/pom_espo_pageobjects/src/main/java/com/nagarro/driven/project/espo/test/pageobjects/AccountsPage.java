package com.nagarro.driven.project.espo.test.pageobjects;

import com.nagarro.driven.client.selenium.SeleniumAbstractDriver;
import com.nagarro.driven.core.reporting.api.KeywordReporting;
import com.nagarro.driven.core.util.Waiter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Paths;

import static java.lang.System.getProperty;

/**
 * AccountsPage contains all the business test related to accounts page and call client actions
 * inside it.
 *
 * @author nagarro
 */
public class AccountsPage {

  /** The web driver client. */
  SeleniumAbstractDriver seleniumClient;



  /* The name of the page. */
  private static final String PAGE_NAME = "AccountsPage";

  /** The accounts page constructor */
  public AccountsPage(SeleniumAbstractDriver client) {
    seleniumClient = client;
  }



  /**
   * It is used to enter the search text in the search field.
   *
   * @param searchText, text to be searched
   */
  @KeywordReporting({"Search text entered is"})
  public void enterSearchData(String searchText) {
    Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "AccountsSearchField").isDisplayed());
    seleniumClient.element(PAGE_NAME, "AccountsSearchField").setText(searchText);
  }

  /** It is used to click the search button. */
  @KeywordReporting()
  public void clickSearchButton() {
    Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "SearchButton").isDisplayed());
    seleniumClient.element(PAGE_NAME, "SearchButton").click();
  }

  /**
   * Search the 'MyHealth' text in the search bar.
   *
   * @param searchText, the text to be searched
   */
  @KeywordReporting()
  public void searchAccounts(String searchText) {
    enterSearchData(searchText);
    clickSearchButton();
  }

  /**
   * Gets the search result for the search text entered.
   *
   * @return the search result
   */
  @KeywordReporting()
  public String getSearchedText() {
    Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "MyHealthSearchResult").isDisplayed());
    return seleniumClient.element(PAGE_NAME, "MyHealthSearchResult").getText();
  }

  /** Clicks the 'MyHealth' option obtained from search results. */
  public void clickMyHealthOption() {
    Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "MyHealthOption").isDisplayed());
    seleniumClient.element(PAGE_NAME, "MyHealthOption").click();
  }

  /**
   * Gets the overview name of the field.
   *
   * @return the overview name
   */
  public String getOverviewDisplayedNameData() {
    Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "OverviewNameDisplay").isDisplayed());
    return seleniumClient.element(PAGE_NAME, "OverviewNameDisplay").getText();
  }

  /**
   * Gets the overview E-mail of the field.
   *
   * @return the overview Billing Address
   */
  public String getOverviewDisplayedBillingAddress() {
    Waiter.waitFor(
        () -> seleniumClient.element(PAGE_NAME, "OverviewBillingAddressDisplay").isDisplayed());
    return seleniumClient.element(PAGE_NAME, "OverviewBillingAddressDisplay").getText();
  }

  /** Clicks on any of the options in searched data, obtained from search results. */
  public void openSearchedData() {
    Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "SearchedAccountsData").isDisplayed());
    seleniumClient.element(PAGE_NAME, "SearchedAccountsData").click();
  }

  /** Clicks the Edit button on Overview page. */
  @KeywordReporting()
  public void clickEditButton() {
    Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "EditButton").isDisplayed());
    seleniumClient.element(PAGE_NAME, "EditButton").click();
  }

  /** Clicks the Save button on Overview page. */
  @KeywordReporting()
  public void clickSaveButton() {
    Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "SaveButton").isDisplayed());
    seleniumClient.element(PAGE_NAME, "SaveButton").click();
  }

  /** Updates the data under Overview section and click on Save button */
  @KeywordReporting({"Email entered ", "Phone entered ", "Website entered "})
  public void updateDetailsInOverview(String billingAddress, String phone, String website) {
    clickEditButton();
    Waiter.waitFor(
        () -> seleniumClient.element(PAGE_NAME, "OverviewBillingAddressField").isDisplayed());
    seleniumClient.element(PAGE_NAME, "OverviewBillingAddressField").setText(billingAddress);
    Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "OverviewPhoneField").isDisplayed());
    seleniumClient.element(PAGE_NAME, "OverviewPhoneField").setText(phone);
    Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "OverviewWebsiteField").isDisplayed());
    seleniumClient.element(PAGE_NAME, "OverviewWebsiteField").setText(website);
    clickSaveButton();
  }

  /** Click on the Account option */
  @KeywordReporting()
  public void clickAccountsOption() {
    Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "AccountOption").isDisplayed());
    seleniumClient.element(PAGE_NAME, "AccountOption").click();
  }

  /** Click on the Account option */
  @KeywordReporting()
  public void searchAndClickUserAccount() {
    Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "USER_ACCOUNT_MONA_MARY").isDisplayed());
    seleniumClient.element(PAGE_NAME, "USER_ACCOUNT_MONA_MARY").click();
  }

  /**
   * Editing the WebSite Url field in the form
   *
   * @param url (to be entered)
   */
  @KeywordReporting()
  public void editWebSiteOnGUI(String url) {
    final String account_website_input = "ACCOUNT_WEBSITE_INPUT";
    Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "EDIT_ACCOUNT_INPUT").isDisplayed());
    seleniumClient.element(PAGE_NAME, "EDIT_ACCOUNT_INPUT").click();
    Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, account_website_input).isDisplayed());
    seleniumClient.element(PAGE_NAME, account_website_input).click();
    Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, account_website_input).isDisplayed());
    seleniumClient.element(PAGE_NAME, account_website_input).setText("");
    Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, account_website_input).isDisplayed());
    seleniumClient.element(PAGE_NAME, account_website_input).setText(url);
    Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "SAVE_ACCOUNT_BUTTON").isDisplayed());
    // save the edit, otherwise it will not be in the DOM
    seleniumClient.element(PAGE_NAME, "SAVE_ACCOUNT_BUTTON").click();
  }

  /** fetching the Url from the field */
  @KeywordReporting()
  public String getWebSiteUrl() {
    Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "WEBSITE_COLUMN_VALUE").isDisplayed());
    return seleniumClient.element(PAGE_NAME, "WEBSITE_COLUMN_VALUE").getText();
  }


}
