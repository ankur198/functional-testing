package com.fab.pageObjects;

import com.nagarro.driven.client.selenium.SeleniumAbstractDriver;
import com.nagarro.driven.core.reporting.api.KeywordReporting;
import com.nagarro.driven.core.util.Waiter;
import com.nagarro.driven.project.espo.test.pageobjects.AccountsPage;
import org.openqa.selenium.StaleElementReferenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
    /**
        * HomePage contains all the business test related to home page and call client actions inside it.
        *
        * @author nagarro
        */
public class HomePage {

    private final Logger log = LoggerFactory.getLogger(com.fab.pageObjects.HomePage.class);

    /** The web driver client. */
    SeleniumAbstractDriver seleniumClient;

    /* The name of the page. */
    private static final String PAGE_NAME = "HomePage1";

    /** The home page constructor */
    public HomePage(SeleniumAbstractDriver client) {
        seleniumClient = client;
    }

    /** Clicks the aws button. */
     @KeywordReporting
    public void clickAwsLink() {
        Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "AwsLink").isDisplayed());
        seleniumClient.element(PAGE_NAME, "AwsLink").click();
    }

        @KeywordReporting
        public void clickAWSAccountsLink() {
            Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "AWSAccounts").isDisplayed());
            seleniumClient.element(PAGE_NAME, "AWSAccounts").click();
        }
        @KeywordReporting
        public void clickNagarroDevOpsButton() {
            Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "NagarroDevOps").isDisplayed());
            seleniumClient.element(PAGE_NAME, "NagarroDevOps").click();
        }

        @KeywordReporting
        public void clickOnAddAwsAccountButton() {
            Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "AddAwsAccount").isDisplayed());
            seleniumClient.element(PAGE_NAME, "AddAwsAccount").click();
        }
        @KeywordReporting
        public void clickOnAddButton() {
            Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "AddButton").isDisplayed());
            seleniumClient.element(PAGE_NAME, "AddButton").click();
        }
        @KeywordReporting
        public void clickOnAnomaly() {
            Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "Anomaly").isDisplayed());
            seleniumClient.element(PAGE_NAME, "Anomaly").click();
        }
        @KeywordReporting
        public void clickCostOptimizationWidget() {
            Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "CostOptimization").isDisplayed());
            seleniumClient.element(PAGE_NAME, "CostOptimization").click();
        }



        /**  Check whether Instance Availability Trend title is displayed or not */
    public boolean isInstanceAvailabilityTrendDisplayed() {
        return Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "InstanceAvailabilityTrend").isDisplayed());
    }

        /**  Check whether Server Locations title is displayed or not */
        public boolean isServerLocationsDisplayed() {
           return Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "ServerLocations").isDisplayed());
        }

        /**  Check whether Security Thread Count For Projects title is displayed or not */
        public boolean isSecurityThreadCountForProjectsDisplayed() {
           return Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "SecurityThreadCountForProjects").isDisplayed());
        }

        /**  Check whether Service vs Security Threats Count title is displayed or not */
        public boolean isServicevsSecurityThreatsCountDisplayed() {
          return Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "ServicevsSecurityThreatsCount").isDisplayed());
        }
        /**  Check whether Instance Availability Trend title is displayed or not */
        public boolean isAccountCostDistributionDisplayed() {
           return Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "AccountCostDistribution").isDisplayed());
        }
        /**  Check whether Instance Availability Trend title is displayed or not */
        public boolean isAWSAccountsDisplayed() {
           return Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "AWSAccounts").isDisplayed());
        }
        /**  Check whether Security Thread Count For Projects title is displayed or not */
        public boolean isAccountInstanceFilteringDisplayed() {
           return Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "SecurityThreadCountForProjects").isDisplayed());
        }

        /**  Check whether Security Threats Count For Regions title is displayed or not */
        public boolean isSecurityThreatsCountForRegionsDisplayed() {
           return Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "SecurityThreatsCountForRegions").isDisplayed());
        }

        /************************* Nagarro Devops Dashboard ***************/

        /**  Check whether "Server Type" title is displayed or not */
        public boolean isServerTypeDisplayed() {
            return Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "ServerType").isDisplayed());
        }
        /**  Check whether "Account Configuration" title is displayed or not */
        public boolean isAccountConfigurationDisplayed() {
            return Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "AccountConfiguration").isDisplayed());
        }
        /**  Check whether "Cost Optimization" title is displayed or not */
        public boolean isCostOptimizationDisplayed() {
            return Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "CostOptimization").isDisplayed());
        }
        /**  Check whether "Service Cost Trend" title is displayed or not */
        public boolean isServiceCostTrendDisplayed() {
            return Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "ServiceCostTrend").isDisplayed());
        }
        /**  Check whether "Tags Dummy" title is displayed or not */
        public boolean isTagsDummyDisplayed() {
            return Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "TagsDummy").isDisplayed());
        }
        /**  Check whether "Security Breaches Dummy" title is displayed or not */
        public boolean isSecurityBreachesDummyDisplayed() {
            return Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "SecurityBreachesDummy").isDisplayed());
        }
        /**  Check whether "Daily Cost Trend" title is displayed or not */
        public boolean isDailyCostTrendDisplayed() {
            return Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "DailyCostTrend").isDisplayed());
        }

        public void enterAccountNameData(String name) {
            Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "AccountName").isDisplayed());
            seleniumClient.element(PAGE_NAME, "AccountName").setText(name);
        }
        public void enterAccountIdData(String id) {
            Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "AccountId").isDisplayed());
            seleniumClient.element(PAGE_NAME, "AccountId").setText(id);
        }
        public void enteAccessKeyData(String key) {
            Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "AccessKey").isDisplayed());
            seleniumClient.element(PAGE_NAME, "AccessKey").setText(key);
        }
        public void enterSecretKeyData(String key) {
            Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "SecretKey").isDisplayed());
            seleniumClient.element(PAGE_NAME, "SecretKey").setText(key);
        }

        public Boolean isSuccessMsgDisplayed() {
           return  Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "SuccessMsg").isDisplayed());

        }

        @KeywordReporting()
        public String getAmiValue() {
            Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "AmiValue").isDisplayed());
            return seleniumClient.element(PAGE_NAME, "AmiValue").getText();
        }
        @KeywordReporting()
        public String getEc2SnapshotValue() {
            Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "Ec2Snapshot").isDisplayed());
            return seleniumClient.element(PAGE_NAME, "Ec2Snapshot").getText();
        }
        @KeywordReporting()
        public String getDbSnapshotValue() {
            Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "DbSnapshot").isDisplayed());
            return seleniumClient.element(PAGE_NAME, "DbSnapshot").getText();
        }

        @KeywordReporting()
        public void clickOnAmi() {
            Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "AmiValue").isDisplayed());
            seleniumClient.element(PAGE_NAME, "AmiValue").click();
        }

        /** Ami name and details*/

        @KeywordReporting()
        public String getAmiTypeValue() {
            Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "AmiType").isDisplayed());
            return seleniumClient.element(PAGE_NAME, "AmiType").getText();
        }
        @KeywordReporting()
        public String getAmiIdValue() {
            Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "AmiId").isDisplayed());
            return seleniumClient.element(PAGE_NAME, "AmiId").getText();
        }
        @KeywordReporting()
        public String getAmiNameValue() {
            Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "AmiName").isDisplayed());
            return seleniumClient.element(PAGE_NAME, "AmiName").getText();
        }
        @KeywordReporting()
        public String getAmiRegionValue() {
            Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "AmiRegion").isDisplayed());
            return seleniumClient.element(PAGE_NAME, "AmiRegion").getText();
        }   @KeywordReporting()
        public String getManagementUrlValue() {
            Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "ManagementUrl").isDisplayed());
            return seleniumClient.element(PAGE_NAME, "ManagementUrl").getText();
        }
        @KeywordReporting()
        public String getSkuValue() {
            Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "Sku").isDisplayed());
            return seleniumClient.element(PAGE_NAME, "Sku").getText();
        }
        @KeywordReporting()
        public String getDateValue() {
            Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "Date").isDisplayed());
            return seleniumClient.element(PAGE_NAME, "Date").getText();
        }




        /** others */

    /**
     * Check whether calendar tile is displayed or not.
     *
     * @return true, if calendar tile is displayed else false
     */
    public boolean isCalenderTileDisplayed() {
        boolean result = false;
        for (int i = 0; i < 2; i++) {
            try {
                if (seleniumClient.element(PAGE_NAME, "CalenderTileTitleText").isDisplayed()) {
                    result = true;
                    break;
                }
            } catch (final StaleElementReferenceException e) {
                log.error(
                        "Exception occured while checking is calender displayed or not: {}", e.getMessage());
            }
        }

        return result;
    }


    /** Click the remove option button. */
    public void clickRemoveOption() {
        try {
            Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "CalenderRemoveOption").isDisplayed());
            seleniumClient.element(PAGE_NAME, "CalenderRemoveOption").click();
            Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "ConfirmationPopup").isDisplayed());
            seleniumClient.element(PAGE_NAME, "ConfirmationPopup").click();
        } catch (final StaleElementReferenceException e) {
            log.error("Exception occured while clicking remove option: {}", e.getMessage());
        }
    }

}
