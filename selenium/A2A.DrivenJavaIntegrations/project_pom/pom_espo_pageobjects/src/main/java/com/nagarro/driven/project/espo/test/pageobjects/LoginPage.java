package com.nagarro.driven.project.espo.test.pageobjects;

import com.nagarro.driven.client.selenium.SeleniumAbstractDriver;
import com.nagarro.driven.core.reporting.api.KeywordReporting;
import com.nagarro.driven.core.util.Waiter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * LoginPage contains all the business test related to login page and call client actions inside it.
 *
 * @author nagarro
 */
public class LoginPage {

  private static final Logger log = LoggerFactory.getLogger(LoginPage.class);
  private static final String LOGIN_BUTTON = "LoginButton";

  /* The name of the page. */
  private static final String PAGE_NAME = "LoginPage";
  /** The web driver client. */
  protected SeleniumAbstractDriver seleniumClient;
  /** The boolean flag for login */
  private boolean isLoggedIn = false;

  /** The login page constructor. */
  public LoginPage(SeleniumAbstractDriver client) {
    seleniumClient = client;
  }

  /**
   * Enter the username in textbox.
   *
   * @param userName, t?he user name value
   */
  @KeywordReporting({"User name is"})
  private void setUserName(String userName) {
    Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "UserNameTextBox").isDisplayed());
    seleniumClient.element(PAGE_NAME, "UserNameTextBox").setText(userName);

  }

  /**
   * Enter the password in text box.
   *
   * @param password password value.
   */
  @KeywordReporting()
  private void setPassword(String password) {
    Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "PasswordTextBox").isDisplayed());
    seleniumClient.element(PAGE_NAME, "PasswordTextBox").setText(password);
  }

  /** Click login button. */
  @KeywordReporting()
  private void clickLogin() {
    Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, LOGIN_BUTTON).isDisplayed());
    seleniumClient.element(PAGE_NAME, LOGIN_BUTTON).click();
  }

  /**
   * Logs in the user to the application.
   *
   * @param userName, username value
   * @param password, password value
   */
  @KeywordReporting()
  public void loginToEspo(String userName, String password) {
    if (isLoginButtonDisplayed()) {
      this.setUserName(userName);
      this.setPassword(password);
      this.clickLogin();
      log.info("User has been logged in to the application");
    }
  }

  /**
   * Get the page title.
   *
   * @return String, the page title.
   */
  @KeywordReporting()
  public String getPageTitle() {
    return seleniumClient.window().getTitle();
  }

  /** Logs out the user from the application. */
  @KeywordReporting()
  public void appLogout() {
    Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "NavigationMenu").isDisplayed());
    seleniumClient.element(PAGE_NAME, "NavigationMenu").click();
    Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "LogoutLink").isDisplayed());
    seleniumClient.element(PAGE_NAME, "LogoutLink").click();
  }

  /**
   * Checks whether login button is displayed or not.
   *
   * @return true, if loin button is displayed else false
   */
  @KeywordReporting()
  public boolean isLoginButtonDisplayed() {
    boolean isLoginDisplayed = false;
    try {
      Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, LOGIN_BUTTON).isDisplayed());
      if (seleniumClient.element(PAGE_NAME, LOGIN_BUTTON).isDisplayed()) {
        isLoginDisplayed = true;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return isLoginDisplayed;
  }

  @Override
  public String toString() {
    return "LoginPage{" + "seleniumClient=" + seleniumClient + ", isLoggedIn=" + isLoggedIn + '}';
  }
}
