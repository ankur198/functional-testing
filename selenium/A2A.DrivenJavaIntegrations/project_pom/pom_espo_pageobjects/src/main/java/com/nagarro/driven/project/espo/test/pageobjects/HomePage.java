package com.nagarro.driven.project.espo.test.pageobjects;

import com.nagarro.driven.client.selenium.SeleniumAbstractDriver;
import com.nagarro.driven.core.reporting.api.KeywordReporting;
import com.nagarro.driven.core.util.Waiter;
import org.openqa.selenium.StaleElementReferenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * HomePage contains all the business test related to home page and call client actions inside it.
 *
 * @author nagarro
 */
public class HomePage {

  private final Logger log = LoggerFactory.getLogger(HomePage.class);

  /** The web driver client. */
  SeleniumAbstractDriver seleniumClient;

  /* The name of the page. */
  private static final String PAGE_NAME = "HomePage";

  /** The home page constructor */
  public HomePage(SeleniumAbstractDriver client) {
    seleniumClient = client;
  }

  /**
   * Clicks the account option.
   *
   * @return the instance of accounts page
   */
  @KeywordReporting()
  public AccountsPage clickAccountsOption() {
    Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "AccountOption").isDisplayed());
    seleniumClient.element(PAGE_NAME, "AccountOption").click();
    return new AccountsPage(seleniumClient);
  }

  /** Add calender on home page. */
  @KeywordReporting()
  public void addCalender() {
    clickAddDashlet();
    clickAddCalender();
  }

  /** Clicks the add dashlet button. */
  public void clickAddDashlet() {
    Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "AddDashlet").isDisplayed());
    seleniumClient.element(PAGE_NAME, "AddDashlet").click();
  }

  /** Clicks add calender button. */
  public void clickAddCalender() {
    Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "AddCalender").isDisplayed());
    seleniumClient.element(PAGE_NAME, "AddCalender").click();
  }

  /**
   * Check whether calendar tile is displayed or not.
   *
   * @return true, if calendar tile is displayed else false
   */
  public boolean isCalenderTileDisplayed() {
    boolean result = false;
    for (int i = 0; i < 2; i++) {
      try {
        if (seleniumClient.element(PAGE_NAME, "CalenderTileTitleText").isDisplayed()) {
          result = true;
          break;
        }
      } catch (final StaleElementReferenceException e) {
        log.error(
            "Exception occured while checking is calender displayed or not: {}", e.getMessage());
      }
    }

    return result;
  }

  /** Click the calendar toggle. */
  public void clickCalenderToggle() {
    Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "CalenderToggleMenu").isDisplayed());
    seleniumClient.element(PAGE_NAME, "CalenderToggleMenu").click();
  }

  /** Click the remove option button. */
  public void clickRemoveOption() {
    try {
      Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "CalenderRemoveOption").isDisplayed());
      seleniumClient.element(PAGE_NAME, "CalenderRemoveOption").click();
      Waiter.waitFor(() -> seleniumClient.element(PAGE_NAME, "ConfirmationPopup").isDisplayed());
      seleniumClient.element(PAGE_NAME, "ConfirmationPopup").click();
    } catch (final StaleElementReferenceException e) {
      log.error("Exception occured while clicking remove option: {}", e.getMessage());
    }
  }

  /** Remove the calendar tile. */
  @KeywordReporting()
  public void removeCalenderTile() {
    clickCalenderToggle();
    clickRemoveOption();
  }
}
